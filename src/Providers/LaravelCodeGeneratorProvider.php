<?php

namespace Duotek\LaravelCodeGenerator\Providers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class LaravelCodeGeneratorProvider extends ServiceProvider
{
    protected array $commands = [
        \Duotek\LaravelCodeGenerator\Console\Commands\Make\Model::class,
        \Duotek\LaravelCodeGenerator\Console\Commands\Make\Resource::class,
        \Duotek\LaravelCodeGenerator\Console\Commands\Make\Service\Service::class,
    ];

    public function register(){
        if (App::environment('local')) {
            $this->commands($this->commands);
        }
    }
}