<?php

namespace Duotek\LaravelCodeGenerator\Console\Exceptions;

class TableNotFoundException extends ForwardMessageToConsoleException {}
