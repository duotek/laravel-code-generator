<?php

namespace Duotek\LaravelCodeGenerator\Console\Exceptions;

use Exception;

class ForwardMessageToConsoleException extends Exception {}